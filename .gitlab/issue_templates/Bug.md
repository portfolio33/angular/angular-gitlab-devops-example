# Resumen

(Da un resumen del issue)

## Pasos para reproducir el error

(Lista los pasos que realizaste hasta encontrar el error)

## Comportamiento deseado

(Indica cual es el comportamiento deseado)

## Comportamiento encontrado

(Indica cual es el comportamiento encontrado)
